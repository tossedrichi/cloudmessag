package com.example.desarrollo3.ofertas;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FCMIdService extends FirebaseInstanceIdService {
    private static final String TAG = "FCMIdService";
    public FCMIdService(){

    }

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        senRegistrationToServer(refreshToken);
    }

    private void senRegistrationToServer(String refreshToken) {
        Log.d(TAG, "senRegistrationToServer: TOKEN "+refreshToken);
    }
}
